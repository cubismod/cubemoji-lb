/* eslint-disable no-undef */
const firebaseConfig = {
  apiKey: 'AIzaSyDJyjzbuHZve6pHSAMcnKsAUOS9l0OE6Bs',
  authDomain: 'cubemoji.firebaseapp.com',
  databaseURL: 'https://cubemoji-default-rtdb.firebaseio.com',
  projectId: 'cubemoji',
  storageBucket: 'cubemoji.appspot.com',
  messagingSenderId: '82109656829',
  appId: '1:82109656829:web:4e517508c40877f49e9894'
}
// Initialize Firebase
firebase.initializeApp(firebaseConfig)

const database = firebase.database()
const leaderboard = database.ref('slots/')
// first we do an inital pull to setup UI
let players = []
// save a reference to player scores so we can easily pull them up
// to create a score log
const playerScore = new Map()
const logMsgs = []

const elementStyling = 'w3-flat-midnight-blue w3-card-4 w3-container w3-round'

leaderboard.orderByChild('score').once('value').then((snapshot) => {
  snapshot.forEach(user => {
    // keep a reference to the user ID so we can create a map later
    const player = user.toJSON()
    player.id = user.key
    player.changed = false
    playerScore.set(player.id, player.score)
    players.push(player)
  })
  const revPlayers = players.reverse()
  const elements = new Map()
  const usrParent = document.getElementById('user-holder')
  const logParent = document.getElementById('feed')
  revPlayers.forEach((player, index) => {
    const element = document.createElement('div')
    element.setAttribute('id', player.key)
    element.setAttribute('class', elementStyling)
    // add index so we can reorder later using flexbox
    element.style.order = index
    element.innerHTML = `${index + 1}. <b>${player.username}</b>: <span class="w3-animate-opacity"><b>${player.score}</b></span> points, <span class="w3-animate-opacity"><b>${(player.timeOnTop / 60).toFixed(2)} min</b></span> on top`
    elements.set(player.id, element)
    usrParent.appendChild(elements.get(player.id))
  })

  leaderboard.orderByChild('score').on('value', (snapshot) => {
    players = []
    // update list on each time
    snapshot.forEach(user => {
      const player = user.toJSON()
      player.id = user.key
      // create score log item
      if (playerScore.get(player.id) !== player.score) {
        const dif = player.score - playerScore.get(player.id)
        const element = document.createElement('div')
        if (dif < 0) {
          element.innerText = `${player.username} lost ${Math.abs(dif)} point(s)`
        } else {
          element.innerText = `${player.username} gained ${Math.abs(dif)} point(s)`
        }
        logMsgs.push(element)
        // add items to the log with new messages at the top
        if (logMsgs.length === 1) {
          logParent.appendChild(element)
        }
        else {
          logParent.insertBefore(element, logMsgs[0])
        }
        // clear out log
        if (logMsgs.length > 40) {
          const toDelete = logMsgs.pop()
          toDelete.remove()
        }
      }
      playerScore.set(player.id, player.score)
      players.push(player)
    })
    const revPlayers = players.reverse()
    revPlayers.forEach((player, index) => {
      const newText = `${index + 1}. <b>${player.username}</b>: <span class="w3-animate-opacity"><b>${player.score}</b></span> point(s), <span class="w3-animate-opacity"><b>${(player.timeOnTop / 60).toFixed(2)} min</b></span> on top`
      if (elements.has(player.id)) {
        // only update element if its already in our list
        const element = elements.get(player.id)
        element.style.order = index
        if (element.innerHTML !== newText) {
          // only update if necessary which will affect animations as well
          element.innerHTML = newText
        }
      } else {
        // create a new element
        const element = document.createElement('div')
        element.setAttribute('id', player.key)
        element.setAttribute('class', elementStyling)
        element.style.order = index
        element.innerHTML = newText
        elements.set(player.id, element)
        usrParent.appendChild(elements.get(player.id))
      }
    })
  })
})
